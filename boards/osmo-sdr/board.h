/* ----------------------------------------------------------------------------
 *         ATMEL Microcontroller Software Support
 * ----------------------------------------------------------------------------
 * Copyright (c) 2008, Atmel Corporation
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer below.
 *
 * Atmel's name may not be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * DISCLAIMER: THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * ----------------------------------------------------------------------------
 */

//------------------------------------------------------------------------------
/// \dir
/// !Purpose
///
/// Definition and functions for using AT91SAM3UE-related features, such
/// has PIO pins, memories, etc.
///
/// !Usage
/// -# The code for booting the board is provided by board_cstartup_xxx.c and
///    board_lowlevel.c.
/// -# For using board PIOs, board characteristics (clock, etc.) and external
///    components, see board.h.
/// -# For manipulating memories (NandFlash, PSRAM, etc.), see board_memories.h.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
/// \unit
/// !Purpose
///
/// Definition of AT91OsmoSDR characteristics, AT91SAM3UE-dependant PIOs and
/// external components interfacing.
///
/// !Usage
/// -# For operating frequency information, see "OsmoSDR - Operating frequencies".
/// -# For using portable PIO definitions, see "OsmoSDR - PIO definitions".
/// -# Several USB definitions are included here (see "OsmoSDR - USB device").
//------------------------------------------------------------------------------

#ifndef BOARD_H
#define BOARD_H

//------------------------------------------------------------------------------
//         Headers
//------------------------------------------------------------------------------

#if defined(at91sam3u4)
    #include "at91sam3u4/chip.h"
    #include "at91sam3u4/AT91SAM3U4.h"
#else
    #error Board does not support the specified chip.
#endif

//------------------------------------------------------------------------------
//         Definitions
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
/// \page "OsmoSDR - Board Description"
/// This page lists several definition related to the board description.
///
/// !Definitions
/// - BOARD_NAME

/// Name of the board.
#define BOARD_NAME "OsmoSDR"
/// Board definition.
#define at91sam3uek
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
/// \page "OsmoSDR - Operating frequencies"
/// This page lists several definition related to the board operating frequency
/// (when using the initialization done by board_lowlevel.c).
///
/// !Definitions
/// - BOARD_MAINOSC
/// - BOARD_MCK

/// Frequency of the board main oscillator.
#define BOARD_MAINOSC           12000000

/// Master clock frequency (when using board_lowlevel.c).
#define BOARD_MCK               48000000

//------------------------------------------------------------------------------
// ADC
//------------------------------------------------------------------------------
/// ADC clock frequency, at 10-bit resolution (in Hz)
#define ADC_MAX_CK_10BIT         5000000
/// Startup time max, return from Idle mode (in �s)
#define ADC_STARTUP_TIME_MAX       15
/// Track and hold Acquisition Time min (in ns)
#define ADC_TRACK_HOLD_TIME_MIN  1200

//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
/// \page "OsmoSDR - USB device"
///
/// !Constants
/// - BOARD_USB_BMATTRIBUTES

/// USB attributes configuration descriptor (bus or self powered, remote wakeup)
#define BOARD_USB_BMATTRIBUTES              USBConfigurationDescriptor_SELFPOWERED_RWAKEUP
//#define BOARD_USB_BMATTRIBUTES            USBConfigurationDescriptor_SELFPOWERED_NORWAKEUP
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
/// \page "OsmoSDR - PIO definitions"
/// This pages lists all the pio definitions contained in board.h. The constants
/// are named using the following convention: PIN_* for a constant which defines
/// a single Pin instance (but may include several PIOs sharing the same
/// controller), and PINS_* for a list of Pin instances.

/// DBGU pins (DTXD and DRXD) definitions, PA11,12.
#define PINS_DBGU  {0x00001800, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_PERIPH_A, PIO_DEFAULT}

/// LED #1 pin definition.
#define PIN_LED_1   {1 << 19, AT91C_BASE_PIOB, AT91C_ID_PIOB, PIO_OUTPUT_1, PIO_DEFAULT}
/// LED #2 pin definition.
#define PIN_LED_2   {1 << 18, AT91C_BASE_PIOB, AT91C_ID_PIOB, PIO_OUTPUT_1, PIO_DEFAULT}
/// List of all LEDs definitions.
#define PINS_LEDS   PIN_LED_1, PIN_LED_2

/// SPI0 to FPGA
/// SPI0 MISO pin definition.
#define PIN_SPI0_MISO  {1 << 13, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_PERIPH_A, PIO_DEFAULT}
/// SPI0 MOSI pin definition.
#define PIN_SPI0_MOSI  {1 << 14, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_PERIPH_A, PIO_DEFAULT}
/// SPI0 SPCK pin definition.
#define PIN_SPI0_SPCK  {1 << 15, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_PERIPH_A, PIO_DEFAULT}
/// SPI0 chip select 0 pin definition.
#define PIN_SPI0_NPCS0_PA16  {1 <<  16, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_PERIPH_A, PIO_DEFAULT}
#define PIN_SPI0_NPCS0  PIN_SPI0_NPCS0_PA16
/// List of SPI0 pin definitions (MISO, MOSI & SPCK).
#define PINS_SPI0      PIN_SPI0_MISO, PIN_SPI0_MOSI, PIN_SPI0_SPCK, PIN_SPI0_NPCS0

/// SSC pins definition.
#define PIN_SSC_TD      {0x1 << 26, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_PERIPH_A, PIO_DEFAULT}
#define PIN_SSC_RD      {0x1 << 27, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_PERIPH_A, PIO_DEFAULT}
#define PIN_SSC_TK      {0x1 << 28, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_PERIPH_A, PIO_DEFAULT}
#define PIN_SSC_RK      {0x1 << 29, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_PERIPH_A, PIO_DEFAULT}
#define PIN_SSC_TF      {0x1 << 30, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_PERIPH_A, PIO_DEFAULT}
#define PIN_SSC_RF      {0x1 << 31, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_PERIPH_A, PIO_DEFAULT}
#define PINS_SSC	PIN_SSC_TD, PIN_SSC_RD, PIN_SSC_TK, PIN_SSC_RK, PIN_SSC_TF, PIN_SSC_RK

/// PCK0
#define PIN_PCK0        {0x1 << 21, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_PERIPH_B, PIO_DEFAULT}

/// TWI pins to Si570 and E4K on Rx board
#define TWI_V3XX
#define PIN_TWI_TWD0    {0x1 << 9, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_PERIPH_A, PIO_DEFAULT}
#define PIN_TWI_TWCK0    {0x1 << 10, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_PERIPH_A, PIO_DEFAULT}
#define PINS_TWI0     PIN_TWI_TWD0, PIN_TWI_TWCK0

/// TWI1 to extension connector
#define PIN_TWI_TWD1    {0x1 << 24, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_PERIPH_A, PIO_DEFAULT}
#define PIN_TWI_TWCK1    {0x1 << 25, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_PERIPH_A, PIO_DEFAULT}
#define PINS_TWI1     PIN_TWI_TWD1, PIN_TWI_TWCK1

/// USART0 on extension connector
#define PIN_USART0_RXD    {0x1 << 19, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_PERIPH_A, PIO_DEFAULT}
#define PIN_USART0_TXD    {0x1 << 18, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_PERIPH_A, PIO_DEFAULT}
#define PIN_USART0_CTS    {0x1 << 8, AT91C_BASE_PIOB, AT91C_ID_PIOB, PIO_PERIPH_A, PIO_DEFAULT}
#define PIN_USART0_RTS    {0x1 << 7, AT91C_BASE_PIOB, AT91C_ID_PIOB, PIO_PERIPH_A, PIO_DEFAULT}
#define PIN_USART0_SCK    {0x1 << 17, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_PERIPH_A, PIO_DEFAULT}

/// USART1
#define PIN_USART1_RXD    {0x1 << 21, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_PERIPH_A, PIO_DEFAULT}
#define PIN_USART1_TXD    {0x1 << 20, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_PERIPH_A, PIO_DEFAULT}
#define PIN_USART1_CTS    {0x1 << 23, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_PERIPH_B, PIO_DEFAULT}
#define PIN_USART1_RTS    {0x1 << 22, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_PERIPH_B, PIO_DEFAULT}
#define PIN_USART1_SCK    {0x1 << 24, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_PERIPH_B, PIO_DEFAULT}

/// USB VBus monitoring pin definition.
#define PIN_USB_VBUS    {1 << 10, AT91C_BASE_PIOB, AT91C_ID_PIOB, PIO_INPUT, PIO_DEFAULT}

/// misc GPIO (default: FPGA/RF: power down)
#define PIN_RFSTBY	{1 << 1, AT91C_BASE_PIOB, AT91C_ID_PIOB, PIO_OUTPUT_0, PIO_DEFAULT}
#define PIN_PDWN	{1 << 0, AT91C_BASE_PIOB, AT91C_ID_PIOB, PIO_OUTPUT_0, PIO_DEFAULT}
#define PIN_INT0	{1 << 17, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_INPUT, PIO_DEFAULT}
#define PIN_FON		{1 << 16, AT91C_BASE_PIOB, AT91C_ID_PIOB, PIO_OUTPUT_0, PIO_DEFAULT}
#define PINS_MISC	PIN_RFSTBY, PIN_PDWN, PIN_INT0, PIN_FON

/// FPGA JTAG
#define PIN_FPGA_TDI	{1 << 8, AT91C_BASE_PIOB, AT91C_ID_PIOB, PIO_OUTPUT_1, PIO_PULLUP}
#define PIN_FPGA_TCK	{1 << 7, AT91C_BASE_PIOB, AT91C_ID_PIOB, PIO_OUTPUT_1, PIO_PULLUP}
#define PIN_FPGA_TDO	{1 << 6, AT91C_BASE_PIOB, AT91C_ID_PIOB, PIO_INPUT, PIO_PULLUP}
#define PIN_FPGA_TMS	{1 << 5, AT91C_BASE_PIOB, AT91C_ID_PIOB, PIO_INPUT, PIO_PULLUP}
#define PINS_FPGA_JTAG	PIN_FPGA_TDI, PIN_FPGA_TCK, PIN_FPGA_TDO, PIN_FPGA_TMS


//------------------------------------------------------------------------------
/// \page "OsmoSDR - Memories"
/// This page lists definitions related to internal & external on-board memories.
///
/// !Internal SRAM
/// - AT91C_ISRAM
/// - AT91C_ISRAM_SIZE

/// !Embedded Flash
/// - AT91C_IFLASH
/// - AT91C_IFLASH_SIZE
/// - AT91C_IFLASH_PAGE_SIZE
/// - AT91C_IFLASH_NB_OF_PAGES
/// - AT91C_IFLASH_LOCK_REGION_SIZE
/// - AT91C_IFLASH_NB_OF_LOCK_BITS
///
/// - AT91C_BASE_EFC
///
/// !PSRAM
/// - BOARD_EBI_PSRAM
/// - BOARD_PSRAM_SIZE

/// Internal SRAM address
#define AT91C_ISRAM                     AT91C_IRAM
#define AT91C_ISRAM_SIZE                AT91C_IRAM_SIZE

#define AT91C_IFLASH                    AT91C_IFLASH0
#define AT91C_IFLASH_SIZE               AT91C_IFLASH0_SIZE
#define AT91C_IFLASH_PAGE_SIZE          AT91C_IFLASH0_PAGE_SIZE
#define AT91C_IFLASH_LOCK_REGION_SIZE   AT91C_IFLASH0_LOCK_REGION_SIZE
#define AT91C_IFLASH_NB_OF_PAGES        AT91C_IFLASH0_NB_OF_PAGES
#define AT91C_IFLASH_NB_OF_LOCK_BITS    AT91C_IFLASH0_NB_OF_LOCK_BITS

/// Indicates chip has an EFC.
#define AT91C_BASE_EFC    AT91C_BASE_EFC0

//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
/// \page "OsmoSDR - Individual chip definition"
/// This page lists the definitions related to different chip's definition
/// located in the board.h file for the OsmoSDR.
///
/// !DBGU
/// - BOARD_DBGU_ID
///
/// !DMA
/// - BOARD_MCI_DMA_CHANNEL
/// - BOARD_LCD_DMA_CHANNEL
/// - BOARD_SSC_DMA_CHANNEL
/// - BOARD_NAND_DMA_CHANNEL
///
/// - DMA_HW_SRC_REQ_ID_MCI0
/// - DMA_HW_DEST_REQ_ID_MCI0
/// - DMA_HW_SRC_REQ_ID_MCI1
/// - DMA_HW_DEST_REQ_ID_MCI1
///
/// - BOARD_SD_DMA_HW_SRC_REQ_ID
/// - BOARD_SD_DMA_HW_DEST_REQ_ID
/// - BOARD_SSC_DMA_HW_SRC_REQ_ID
/// - BOARD_SSC_DMA_HW_DEST_REQ_ID
///
/// !RTC
/// - BOARD_RTC_ID
///
/// !Twi eeprom
/// -BOARD_ID_TWI_EEPROM
/// -BOARD_BASE_TWI_EEPROM
/// -BOARD_PINS_TWI_EEPROM
///
/// !USART
/// -BOARD_PIN_USART_RXD
/// -BOARD_PIN_USART_TXD
/// -BOARD_PIN_USART_CTS
/// -BOARD_PIN_USART_RTS
/// -BOARD_USART_BASE
/// -BOARD_ID_USART
///

/// DBGU
#define BOARD_DBGU_ID               AT91C_ID_DBGU

/// Dma channel number
#define BOARD_SSC_DMA_CHANNEL                         2
/// SSC DMA hardware handshaking ID
#define BOARD_SSC_DMA_HW_SRC_REQ_ID      AT91C_HDMA_SRC_PER_4
#define BOARD_SSC_DMA_HW_DEST_REQ_ID     AT91C_HDMA_DST_PER_3

/// USART
#define BOARD_PIN_USART_RXD        PIN_USART1_RXD
#define BOARD_PIN_USART_TXD        PIN_USART1_TXD
#define BOARD_PIN_USART_CTS        PIN_USART1_CTS
#define BOARD_PIN_USART_RTS        PIN_USART1_RTS
#define BOARD_USART_BASE           AT91C_BASE_US1
#define BOARD_ID_USART             AT91C_ID_US1

/// Interrupt source
typedef enum IRQn
{
/******  Cortex-M3 Processor Exceptions Numbers ***************************************************/
  NonMaskableInt_IRQn         = -14,    /*!< 2 Non Maskable Interrupt                             */
  MemoryManagement_IRQn       = -12,    /*!< 4 Cortex-M3 Memory Management Interrupt              */
  BusFault_IRQn               = -11,    /*!< 5 Cortex-M3 Bus Fault Interrupt                      */
  UsageFault_IRQn             = -10,    /*!< 6 Cortex-M3 Usage Fault Interrupt                    */
  SVCall_IRQn                 = -5,     /*!< 11 Cortex-M3 SV Call Interrupt                       */
  DebugMonitor_IRQn           = -4,     /*!< 12 Cortex-M3 Debug Monitor Interrupt                 */
  PendSV_IRQn                 = -2,     /*!< 14 Cortex-M3 Pend SV Interrupt                       */
  SysTick_IRQn                = -1,     /*!< 15 Cortex-M3 System Tick Interrupt                   */

/******  AT91SAM3U4 specific Interrupt Numbers *********************************************************/
 IROn_SUPC                = AT91C_ID_SUPC , // SUPPLY CONTROLLER
 IROn_RSTC                = AT91C_ID_RSTC , // RESET CONTROLLER
 IROn_RTC                 = AT91C_ID_RTC  , // REAL TIME CLOCK
 IROn_RTT                 = AT91C_ID_RTT  , // REAL TIME TIMER
 IROn_WDG                 = AT91C_ID_WDG  , // WATCHDOG TIMER
 IROn_PMC                 = AT91C_ID_PMC  , // PMC
 IROn_EFC0                = AT91C_ID_EFC0 , // EFC0
 IROn_EFC1                = AT91C_ID_EFC1 , // EFC1
 IROn_DBGU                = AT91C_ID_DBGU , // DBGU
 IROn_HSMC4               = AT91C_ID_HSMC4, // HSMC4
 IROn_PIOA                = AT91C_ID_PIOA , // Parallel IO Controller A
 IROn_PIOB                = AT91C_ID_PIOB , // Parallel IO Controller B
 IROn_PIOC                = AT91C_ID_PIOC , // Parallel IO Controller C
 IROn_US0                 = AT91C_ID_US0  , // USART 0
 IROn_US1                 = AT91C_ID_US1  , // USART 1
 IROn_US2                 = AT91C_ID_US2  , // USART 2
 IROn_US3                 = AT91C_ID_US3  , // USART 3
 IROn_MCI0                = AT91C_ID_MCI0 , // Multimedia Card Interface
 IROn_TWI0                = AT91C_ID_TWI0 , // TWI 0
 IROn_TWI1                = AT91C_ID_TWI1 , // TWI 1
 IROn_SPI0                = AT91C_ID_SPI0 , // Serial Peripheral Interface
 IROn_SSC0                = AT91C_ID_SSC0 , // Serial Synchronous Controller 0
 IROn_TC0                 = AT91C_ID_TC0  , // Timer Counter 0
 IROn_TC1                 = AT91C_ID_TC1  , // Timer Counter 1
 IROn_TC2                 = AT91C_ID_TC2  , // Timer Counter 2
 IROn_PWMC                = AT91C_ID_PWMC , // Pulse Width Modulation Controller
 IROn_ADCC0               = AT91C_ID_ADC12B, // ADC controller0
 IROn_ADCC1               = AT91C_ID_ADC, // ADC controller1
 IROn_HDMA                = AT91C_ID_HDMA , // HDMA
 IROn_UDPHS               = AT91C_ID_UDPHS // USB Device High Speed
} IRQn_Type;  

//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// DFU

#define BOARD_USB_DFU
#define BOARD_DFU_BTN_PIOA	12
#define BOARD_DFU_BOOT_SIZE	(16 * 1024)
#define BOARD_DFU_PAGE_SIZE	512
#define BOARD_DFU_NUM_IF	4

#define BOARD_USB_VENDOR	0x16c0
#define BOARD_USB_PRODUCT	0x0763
#define BOARD_USB_RELEASE	0x0001


#endif //#ifndef BOARD_H

