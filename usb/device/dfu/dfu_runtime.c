
#include <board.h>
#include <core_cm3.h>

#include <usb/device/core/USBD.h>
#include <usb/device/dfu/dfu.h>


void DFURT_SwitchToDFU(void)
{
	unsigned int *dfu_except_tbl = (unsigned int *)AT91C_IFLASH0;
	void (*toDFU)(void) = (void *)dfu_except_tbl[1];

	*(unsigned int *)USB_DFU_MAGIC_ADDR = USB_DFU_MAGIC;

	USBD_Disconnect();
	__disable_irq();

	toDFU();
}
